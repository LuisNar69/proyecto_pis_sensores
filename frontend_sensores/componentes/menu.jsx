'use client'

import { borrarSesion, getRol } from "@/hooks/SessionUtilClient";
import Cookies from "js-cookie";
import Link from "next/link";

export default function Menu() {
    const rol = getRol();

    const salir = () => {
        Cookies.remove("token");
        borrarSesion();
    }

    return (
        <nav className="navbar navbar-expand bg-primary">
            <div className="container-fluid">
                <img src="https://siaaf.unl.edu.ec/static/img/logo.png" width={180} alt="logo" />

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auhref mb-2 mb-lg-0">
                        <li className="nav-item">
                            <Link className="nav-link active text-white " aria-current="page" href="/principal">Inicio</Link>
                        </li>

                        {rol === "Administrador" && (
                            <>
                                <li className="nav-item">
                                    <Link className="nav-link active text-white" aria-current="page" href="/usuarios">Gestion Usuarios</Link>
                                </li>

                                <li className="nav-item">
                                    <Link className="nav-link active text-white" aria-current="page" href="/sensores">Gestion Sensores</Link>
                                </li>

                                <li className="nav-item">
                                    <Link className="nav-link active text-white" aria-current="page" href="/configuracion">ESP32 Maestro</Link>
                                </li>
                            </>
                        )}

                        <li className="nav-item">
                            <Link className="nav-link active text-white" aria-current="page" href="/pronosticos">Pronósticos</Link>
                        </li>

                        <li className="nav-item">
                            <Link className="nav-link active text-white" aria-current="page" href="/informacion">Ayuda</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link active text-white" aria-current="page" href="/login" onClick={salir}>Salir</Link>
                        </li>
                    </ul>
                </div>
                <label className="text-white" style={{ marginRight: 10 }}>perfil</label>
                <div className="dropdown">
                    <Link href="/perfilUsuario" style={{ display: 'flex', flexDirection: 'row-reverse', alignItems: 'center' }}>
                        <img src="/img/avatar.png" className="rounded-circle" height="70" />
                    </Link>
                </div>
            </div>
            <br />
            <br />
        </nav>
    );
}
