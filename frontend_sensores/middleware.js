import { NextResponse } from 'next/server'


export function middleware(request) {
    let tokenValue = false;
    const tokenObject = request.cookies.get('token');

    if (tokenObject) {
        tokenValue = JSON.parse(tokenObject.value);
    }

    if (!tokenValue && request.nextUrl.pathname.startsWith('/principal')) {
        return NextResponse.redirect(new URL("/login", request.url));
    }

    if (!tokenValue && request.nextUrl.pathname.startsWith('/usuarios')) {
        return NextResponse.redirect(new URL("/login", request.url));
    }

    if (!tokenValue && request.nextUrl.pathname.startsWith('/sensores')) {
        return NextResponse.redirect(new URL("/login", request.url));
    }

    if (!tokenValue && request.nextUrl.pathname.startsWith('/pronosticos')) {
        return NextResponse.redirect(new URL("/login", request.url));
    }

    if (!tokenValue && request.nextUrl.pathname.startsWith('/perfilUsuario')) {
        return NextResponse.redirect(new URL("/login", request.url));
    }

    if (!tokenValue && request.nextUrl.pathname.startsWith('/configuracion')) {
        return NextResponse.redirect(new URL("/login", request.url));
    }

    if (tokenValue && request.nextUrl.pathname.startsWith('/login')) {
        return NextResponse.redirect(new URL('/principal', request.url))
    }
}

// See "Matching Paths" below to learn more
export const config = {
    matcher: ['/usuarios(.*)', '/pronosticos(.*)', '/sensores(.*)',
        '/login', '/principal', '/perfilUsuario', '/configuracion'],
}