'use client'
import Link from "next/link";
import Menu from '@/componentes/menu';
import Footer from '@/componentes/footer';
import { useState } from "react";
import { busquedaSensores, cambiarEstado, obtenerData } from "@/hooks/Conexion";
import Cookies from "js-cookie";
import { useRouter } from "next/navigation";
import mensajes from "@/componentes/Mensajes";
import { borrarSesion, getRol, getToken } from "@/hooks/SessionUtilClient";

export default function Page() {
    const key = getToken();
    const rol = getRol();

    const router = useRouter();
    const [sensores, setSensores] = useState([]);
    const [llamada, setLlamada] = useState(false);
    const [valorBusqueda, setValorBusqueda] = useState('');

    const handleValorBusqueda = (event) => {
        setValorBusqueda(event.target.value);
    };

    //llamar sensores
    if (!llamada) {
        obtenerData('admin/sensores', key, rol).then((info) => {
            if (info.code === 200) {
                setSensores(info.datos);
                setLlamada(true);
            } else if (info.code !== 200 && (info.tag === "token expirado o no valido" || info.tag === "token no valido" || info.tag === "no existe token")) {
                mensajes(info.tag, "Error", "error");
                Cookies.remove("token");
                borrarSesion();
                router.push("/login")
            } else if (info.code !== 200 && info.tag === "Acceso no autorizado") {
                router.push("/principal")
                mensajes(info.tag, "Informacion", "error");
            } else {
                mensajes("No se pudo Listar los documentos", "Error", "error");
            }
        });
    };

    //cambiar estado sensor
    const handleEstado = (external, dato) => {

        let datos = {
            'estado': dato,
        }

        cambiarEstado('admin/sensores/estado/' + external, datos, key, rol).then((info) => {

            if (info.code === 200) {
                mensajes(info.tag, "Informacion", "success");
                setLlamada(false);
            } else if (info.code !== 200 && (info.tag === "token expirado o no valido" || info.tag === "token no valido" || info.tag === "no existe token")) {
                mensajes(info.tag, "Error", "error");
                Cookies.remove("token");
                borrarSesion();
                router.push("/login")
            } else if (info.code !== 200 && info.tag === "Acceso no autorizado") {
                router.push("/principal")
                mensajes(info.tag, "Informacion", "error");
            } else {
                mensajes(info.tag, "Error", "error");
            }
        });
    };

    //Metodo para busqueda y listado de datos
    const handleBuscarClick = () => {

        const selectElement = document.getElementById('filtroBusqueda');
        const selectedValue = selectElement.value;
        let dir = "";

        if (selectedValue === "tipo_sensor") {
            if (valorBusqueda.length === 0) {
                mensajes("ingresa algun valor de busqueda", "Informacion", "error");
                return;
            } else {
                dir = "admin/sensores/buscar/tipo/" + valorBusqueda.toUpperCase();
            }
        } else if (selectedValue === "nombre") {
            if (valorBusqueda.length === 0) {
                mensajes("ingresa algun valor de busqueda", "Informacion", "error");
                return;
            } else {
                dir = "admin/sensores/buscar/nombre/" + valorBusqueda;
            }

        } else {
            dir = "admin/sensores/";
        }

        busquedaSensores(dir, key, rol).then((info) => {
            if (info.code === 200) {
                setSensores(info.datos);
            } else if (info.code !== 200 && (info.tag === "token expirado o no valido" || info.tag === "token no valido" || info.tag === "no existe token")) {
                mensajes(info.tag, "Error", "error");
                Cookies.remove("token");
                borrarSesion();
                router.push("/login")
            } else if (info.code !== 200 && info.tag === "Acceso no autorizado") {
                router.push("/principal")
                mensajes(info.tag, "Informacion", "error");
            } else {
                mensajes("No se pudo listar los datos", "Error", "error");
            }
        });
    };

    return (
        <div className="row">
            <Menu />
            <h1 style={{ textAlign: "center" }}>Sensores</h1>

            <div className="input-group mb-3 ">
                <select className="form-control w-20" id="filtroBusqueda" style={{ backgroundColor: '#F2F6F5', color: '#333', border: "1px solid #000000" }} aria-describedby="button-addon2">
                    <option value="">Todo</option>
                    <option value="tipo_sensor">Tipo Sensor</option>
                    <option value="nombre">Nombre Sensor</option>

                </select>
                <input type="text" className="form-control w-50" placeholder="Ingrese dato de busqueda" onChange={handleValorBusqueda} value={valorBusqueda} />
                <button className="btn btn-secondary " type="button" id="button-addon2" onClick={handleBuscarClick}>Buscar</button>
            </div>


            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-12 mb-4 text-center">
                        <div className="btn-group" role="group">
                            <Link href="/sensores/registrar" className="btn btn-success font-weight-bold">Registrar</Link>
                        </div>
                    </div>
                </div>
            </div>

            <div className="col-12">
                <div style={{ maxHeight: '350px', overflowY: 'auto' }}>
                    <table className="table table-bordered table-hover" >
                        <thead className="table-active">
                            <tr style={{ textAlign: "center", backgroundColor: "black", color: "white" }}>
                                <th>NOMBRE</th>
                                <th>TIPO</th>
                                <th>UBICACION</th>
                                <th>DIRECCION IP</th>
                                <th>IMG</th>
                                <th>ESTADO</th>
                                <th>ACCIONES</th>
                            </tr>
                        </thead>
                        <tbody>
                            {sensores && sensores.length > 0 ? (
                                sensores.map((dato, i) => (
                                    <tr key={i}>
                                        <td>{dato.nombre}</td>
                                        <td>{dato.tipo_sensor}</td>
                                        <td>{dato.ubicacion}</td>
                                        <td>{dato.ip}</td>
                                        <td><img style={{ width: 50, height: 50 }} src={dato.img} /></td>
                                        <td>{dato.estado === true ? 'Activo' : 'Inactivo'}</td>
                                        <td>
                                            {<Link style={{ margin: "5px" }} href={`sensores/modificar/${dato.external_id}`} className="btn btn-warning font-weight-bold">Modificar</Link>}
                                            {dato.estado ? (
                                                <button type="button" onClick={() => handleEstado(dato.external_id, "false")} style={{ margin: "5px" }} className="btn btn-danger font-weight-bold"> Desactivar  </button>
                                            ) : (
                                                <button type="button" onClick={() => handleEstado(dato.external_id, "true")} style={{ margin: "5px" }} className="btn btn-success font-weight-bold"> Activar </button>
                                            )}
                                        </td>
                                    </tr>
                                )
                                )
                            ) : (
                                <tr>
                                    <td colSpan="7">No se dispone de información</td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                </div>
            </div>
            <Footer />
        </div>

    );
}