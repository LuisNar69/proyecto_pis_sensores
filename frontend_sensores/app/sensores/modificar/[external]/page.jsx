"use client";

import * as Yup from 'yup';
import Link from "next/link";
import Menu from '@/componentes/menu';
import Footer from '@/componentes/footer';
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm } from 'react-hook-form';
import { useRouter } from 'next/navigation';
import Cookies from 'js-cookie';
import { useEffect, useState } from 'react';
import mensajes from '@/componentes/Mensajes';
import { guardarSensor, obtenerData } from '@/hooks/Conexion';
import { borrarSesion, getRol, getToken } from '@/hooks/SessionUtilClient';

export default function Page({ params }) {
    const { external } = params;
    const key = getToken();
    const rol = getRol();

    const router = useRouter();
    const [sensor, setSensor] = useState([]);
    const [llamada, setLlamada] = useState(false);

    const [selectedImage, setSelectedImage] = useState([]);

    //Validaciones de campos
    const validationShema = Yup.object().shape({
        nombre: Yup.string().required('Ingrese nombre del sensor'),
        ubicacion: Yup.string().required('Ingrese una ubicacion'),
        ip: Yup.string().test('is-valid-ip', 'Ingrese una dirección IP válida', function (value) {
            return /^(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)$/.test(value);
        }).required('Ingrese una dirección IP'),
        tipo: Yup.string().required('Seleccione un tipo'),
        img: Yup.mixed().required('Seleccione una imagen'),
    });

    const formOptions = { resolver: yupResolver(validationShema) };
    const { register, handleSubmit, formState, setValue } = useForm(formOptions);
    const { errors } = formState;

    //buscar sensores
    if (!llamada) {
        obtenerData("admin/sensores/obtener/" + external, key, rol).then((info) => {
            if (info.code === 200) {
                setSensor(info.datos);
                setLlamada(true);
            } else if (info.code !== 200 && (info.tag === "token expirado o no valido" || info.tag === "token no valido" || info.tag === "no existe token")) {
                mensajes(info.tag, "Error", "error");
                Cookies.remove("token");
                borrarSesion();
                router.push("/login")
            } else if (info.code !== 200 && info.tag === "Acceso no autorizado") {
                router.push("/principal")
                mensajes(info.tag, "Informacion", "error");
            } else {
                mensajes("No se pudo encontrar el sensor", "Error", "error");
            }
        });
    };

    const handleImageChange = (event) => {
        const file = event.target.files;

        setSelectedImage(file);
    };

    //Metodo para guardar Sensores
    const sendData = (data) => {

        var datos = {
            'nombre': data.nombre,
            'ubicacion': data.ubicacion,
            'ip': data.ip,
            'tipo': data.tipo,
        };

        guardarSensor('admin/sensores/modificar/' + external, selectedImage, datos, key, rol).then((info) => {
            if (info.code !== 200) {
                if (info.code !== 200 && (info.tag === "token expirado o no valido" || info.tag === "token no valido" || info.tag === "no existe token")) {
                    mensajes(info.tag, "Error", "error");
                    Cookies.remove("token");
                    borrarSesion();
                    router.push("/login")
                } else {
                    mensajes(info.tag, "Error", "error")
                }
            } else {
                mensajes(info.tag, "Informacion", "success")
                router.push("/sensores");
            }
        });
    };

    useEffect(() => {
        if (sensor) {
            setValue('nombre', sensor.nombre);
            setValue('ubicacion', sensor.ubicacion);
            setValue('ip', sensor.ip);
            setValue('tipo', sensor.tipo_sensor);
            if (tipo === '' || tipo === undefined || tipo === null) {
                setTipo(sensor.tipo);
            }
        }
    }, [sensor, setValue]);

    return (
        <div className="row">
            <Menu />
            <center>
                <div className="d-flex flex-column" style={{ width: 700 }}>

                    <h1 style={{ textAlign: "center", fontSize: "1.5em" }}>Modificar Sensor</h1>

                    <div className='container-fluid' style={{ border: '4px solid #ccc', padding: '20px', borderRadius: '10px', maxWidth: '1000px', margin: 'auto' }}>
                        <div className="container-fluid" >
                            <img className="card" src="/img/sensor.png" style={{ width: 90, height: 90 }} />
                        </div>
                        <br />
                        <form className="user" onSubmit={handleSubmit(sendData)}>

                            {/*Ingresar nombre y ubicacion*/}
                            <div className="row mb-4">
                                <div className="col">
                                    <input {...register('nombre')} name="nombre" id="nombre" className={`form-control ${errors.nombre ? 'is-invalid' : ''}`} placeholder='Ingrese el nombre' />
                                    <div className='alert alert-danger invalid-feedback'>{errors.nombre?.message}</div>
                                </div>
                                <div className="col">
                                    <input {...register('ubicacion')} name="ubicacion" id="ubicacion" className={`form-control ${errors.ubicacion ? 'is-invalid' : ''}`} placeholder='Ingrese la ubicacion' />
                                    <div className='alert alert-danger invalid-feedback'>{errors.ubicacion?.message}</div>
                                </div>
                            </div>

                            {/*Ingresar descripcion y tipo*/}

                            <div className="row mb-4">
                                <div className="col">
                                    <input {...register('ip')} name="ip" id="ip" className={`form-control ${errors.ip ? 'is-invalid' : ''}`} placeholder='Ingrese una direccion ip' />
                                    <div className='alert alert-danger invalid-feedback'>{errors.ip?.message}</div>
                                </div>
                                <div className="col">
                                    <select {...register('tipo')} name="tipo" id="tipo" className={`form-control ${errors.tipo ? 'is-invalid' : ''}`} >
                                        <option >Elija un Tipo</option>
                                        <option >TEMPERATURA</option>
                                        <option >HUMEDAD</option>
                                        <option >VIENTO</option>
                                        <option >PRECIPITACION</option>
                                    </select>
                                    <div className='alert alert-danger invalid-feedback'>{errors.tipo?.message}</div>
                                </div>
                            </div>

                            {/*Ingresar Imagen*/}

                            <div className="row mb-4">
                                <div className="col">
                                    <div className="col">
                                        Imagen <input {...register('img')} type="file" accept="image/jpeg, image/png, image/jpg" className={`form-control ${errors.img ? 'is-invalid' : ''}`} onChange={handleImageChange} />
                                        <div className='alert alert-danger invalid-feedback'>{errors.img?.message}</div>
                                    </div>
                                </div>
                            </div>

                            <Link href="/sensores" className="btn btn-danger" style={{ flex: '1', marginRight: '4px' }}>
                                Cancelar
                            </Link>
                            <button type="submit" className="btn btn-success" style={{ flex: '1', marginLeft: '4px' }}>
                                Guardar
                            </button>
                        </form>
                    </div>
                </div>
            </center>
            <br />
            <Footer />
        </div>

    );
}