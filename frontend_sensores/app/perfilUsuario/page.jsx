'use client';
import Link from "next/link";
import { useRouter } from 'next/navigation';
import { borrarSesion, getExternal, getRol, getToken } from "@/hooks/SessionUtilClient";
import Menu from '@/componentes/menu';
import Footer from '@/componentes/footer';
import Cookies from "js-cookie";
import { cambiarEstado, obtenerP } from "@/hooks/Conexion";
import mensajes from "@/componentes/Mensajes";
import { useState } from "react";

export default function Page() {

  const key = getToken();
  const external = getExternal();
  const rol = getRol();

  const [persona, setPersona] = useState([]);
  const [obt, setObt] = useState(false);
  const router = useRouter();

  //obtener los datos de las personas por el external
  if (!obt) {
    obtenerP("admin/usuario/buscar/" + external, key, rol).then((info) => {
      if (info.code === 200) {
        setPersona(info.datos);
        setObt(true);
      } else if (info.code !== 200 && (info.tag === "token expirado o no valido" || info.tag === "token no valido" || info.tag === "no existe token")) {
        mensajes(info.tag, "Error", "error");
        Cookies.remove("token");
        borrarSesion();
        router.push("/login")
      } else if (info.code !== 200 && info.tag === "Acceso no autorizado") {
        router.push("/principal")
        mensajes(info.tag, "Informacion", "error");
      } else {
        mensajes("No se pudo obtener los datos de la persona", "Error", "error");
      }
    });
  };

  //cambiar estado sensor
  const handleEliminar = () => {

    let datos = {
      "estado": "false"
    }

    cambiarEstado('admin/usuario/estado/' + external, datos, key, rol).then((info) => {
      if (info.code === 200) {
        mensajes(info.tag, "Success", "Informacion");
        Cookies.remove("token");
        borrarSesion();
        router.push("/login");
      } else if (info.code !== 200 && (info.tag === "token expirado o no valido" || info.tag === "token no valido" || info.tag === "no existe token")) {
        mensajes(info.tag, "Error", "error");
        Cookies.remove("token")
        borrarSesion();
        router.push("/login")
      } else if (info.code !== 200 && info.tag === "Acceso no autorizado") {
        router.push("/principal")
        mensajes(info.tag, "Informacion", "error");
      }
    });
  };

  return (
    <div className="row" >
      <Menu />
      <center>
        <div className="container py-5 h-100">
          <div className="row d-flex justify-content-center align-items-center h-100">
            <div className="col col-lg-8 mb-4 mb-lg-0">
              <div className="card mb-3" style={{ borderRadius: ".5rem" }}>
                <div className="row g-0">
                  <div className="col-md-4 gradient-custom text-center text-white"
                    style={{ border: '0.5rem', borderBottomLeftRadius: '0.5rem', background: `linear-gradient(to right bottom, rgba(174, 214, 241 ), rgba(253, 160, 133, 1))` }}>
                    <img src="/img/user.png"
                      alt="Avatar" className="img-fluid my-5" style={{ width: "80px" }} />
                    <h5>{persona.nombres + " " + persona.apellidos}</h5>
                    <p>{rol}</p>
                    <i className="far fa-edit mb-5"></i>
                    <Link href={`/usuarios/modificar/${external}`}>
                      <img src="/img/iconEdit.jpg" style={{ width: "40px" }} />
                    </Link>
                  </div>
                  <div className="col-md-8 " style={{ background: `linear-gradient(to right bottom, rgba(253, 254, 254 ), rgba(235, 245, 251))` }}>
                    <div className="card-body p-4">
                      <h6>Informacion Completa</h6>
                      <hr className="mt-0 mb-4" />
                      <div className="row pt-1">
                        <div className="col-6 mb-3">
                          <h6>Nombres</h6>
                          <p className="text-muted">{persona.nombres}</p>
                        </div>
                        <div className="col-6 mb-3">
                          <h6>Apellidos</h6>
                          <p className="text-muted">{persona.apellidos}</p>
                        </div>
                      </div>
                      <div className="row pt-1">
                        <div className="col-6 mb-3">
                          <h6>Telefono</h6>
                          <p className="text-muted">{persona.telefono}</p>
                        </div>
                        <div className="col-6 mb-3">
                          <h6>Cedula</h6>
                          <p className="text-muted">{persona.cedula}</p>
                        </div>
                        <div className="row pt-1">
                          <div className="col-6 mb-3">
                            <h6>Email</h6>
                            <p className="text-muted">{persona.cuenta?.correo}</p>
                          </div>
                          <div className="col-6 mb-3">
                            <h6>Estado Cuenta</h6>
                            <p className="text-muted">{persona.cuenta?.estado === true ? 'Activo' : 'Inactivo'} </p>
                          </div>
                        </div>
                      </div>
                      {rol === "Usuario" && (
                        <>
                          <button type="button"
                            onClick={() => {
                              const confirma = window.confirm("¿Estás seguro de que deseas dar de baja la cuenta?");
                              if (confirma) {
                                handleEliminar();
                              }
                            }}
                            style={{ margin: "5px" }} className="btn btn-danger font-weight-bold"> Dar de baja cuenta </button>
                        </>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </center>
      <br />
      <Footer />
    </div>

  );

}

