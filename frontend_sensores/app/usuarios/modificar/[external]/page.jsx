'use client';
import mensajes from "@/componentes/Mensajes";
import * as Yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm } from 'react-hook-form';
import Link from "next/link";
import { useRouter } from 'next/navigation';
import { guardar, obtenerP, obtenerR } from "@/hooks/Conexion";
import { useEffect, useState } from "react";
import { borrarSesion, getRol, getToken } from "@/hooks/SessionUtilClient";
import Menu from '@/componentes/menu';
import Footer from '@/componentes/footer';
import Cookies from "js-cookie";

export default function Page({ params }) {
  const { external } = params;
  const key = getToken();
  const rol = getRol();

  const router = useRouter();
  const [roles, setRoles] = useState([]);
  const [persona, setPersona] = useState([]);
  const [obt, setObt] = useState(false);
  const [obt2, setObt2] = useState(false);

  const validationShema = Yup.object().shape({
    nombres: Yup.string().required('Ingrese los nombres'),
    apellidos: Yup.string().required('ingrese los apellidos'),
    cedula: Yup.string().required('Ingrese un nro cedula'),
    telefono: Yup.string().required('ingrese un telefono'),
    rol: Yup.string().required('Seleccione un rol'),
    correo: Yup.string().required('Ingrese un correo electronico').email('Se requiere correo valido').test("error", 'Debe ingresar un correo institucional de la UNL   (unl.edu.ec)', (value) => {
      if (value) {
        const dominio = value.split('@')[1];
        return dominio === 'unl.edu.ec';
      }
      return false;
    }),

  });

  const formOptions = { resolver: yupResolver(validationShema) };
  const { register, handleSubmit, setValue, formState } = useForm(formOptions);
  const { errors } = formState;

  //obtener los datos de las personas por el external
  if (!obt) {
    obtenerP("admin/usuario/buscar/" + external, key, rol).then((info) => {
      if (info.code === 200) {
        const PersonaD = info.datos;
        setPersona(PersonaD);
        setObt(true);
      } else if (info.code !== 200 && (info.tag === "token expirado o no valido" || info.tag === "token no valido" || info.tag === "no existe token")) {
        mensajes(info.tag, "Error", "error");
        Cookies.remove("token");
        borrarSesion();
        router.push("/login")
      } else if (info.code !== 200 && info.tag === "Acceso no autorizado") {
        router.push("/principal")
        mensajes(info.tag, "Informacion", "error");
      } else {
        mensajes("No se pudo obtener los datos de la persona", "Error", "error");
      }
    });
  };

  //se obtienen los datos del usuario a modificar
  useEffect(() => {

    setValue('nombres', persona.nombres);
    setValue('apellidos', persona.apellidos);
    setValue('telefono', persona.telefono);
    setValue('cedula', persona.cedula);
    setValue('correo', persona.cuenta?.correo);
    setValue('rol', persona.rol?.external_id);

  }, [persona, setValue]);

  //obtener el rol para listar en cbx
  if (!obt2) {
    obtenerR('admin/rol', key, rol).then((info) => {

      if (info.code === 200) {
        setRoles(info.datos);
        setObt2(true);
      } else if (info.code !== 200 && (info.tag === "token expirado o no valido" || info.tag === "token no valido" || info.tag === "no existe token")) {
        mensajes(info.tag, "Error", "error");
        Cookies.remove("token");
        borrarSesion();
        router.push("/login")
      } else if (info.code !== 200 && info.tag === "Acceso no autorizado") {
        router.push("/principal")
        mensajes(info.tag, "Informacion", "error");
      } else {
        mensajes("No se pudo Listar los roles", "Error", "error");
      }
    });
  };

  //enviar datos para  modificar
  const sendData = (data) => {

    var datos = {
      'nombres': data.nombres,
      'apellidos': data.apellidos,
      'telefono': data.telefono,
      'cedula': data.cedula,
      'rol': data.rol,
      'clave': data.clave,
      'correo': data.correo
    };

    guardar('admin/usuario/modificar/' + external, datos, key, rol).then((info) => {
      if (info.code === 200) {
        mensajes("Usuario modificado correctamente", "Informacion", "success")
        if (rol === "Administrador") {
          router.push("/usuarios");
        } else {
          router.push("/perfilUsuario");
        }
      } else if (info.code !== 200 && (info.tag === "token expirado o no valido" || info.tag === "token no valido" || info.tag === "no existe token")) {
        mensajes(info.tag, "Error", "error");
        Cookies.remove("token");
        borrarSesion();
        router.push("/login")
      } else if (info.code !== 200 && info.tag === "Acceso no autorizado") {
        router.push("/principal")
        mensajes(info.tag, "Informacion", "error");
      } else {
        mensajes("El usuario no se pudo modificar", "Error", "error");
      }
    });
  };

  return (
    <div className="wrapper" >
      <Menu />
      <center>
        <div className="d-flex flex-column" style={{ width: 700 }}>
          <h1 style={{ textAlign: "center", fontSize: "1.5em" }}>Modificar Usuario</h1>
          <div className='container-fluid' style={{ border: '4px solid #ccc', padding: '20px', borderRadius: '10px', maxWidth: '1000px', margin: 'auto' }}>

            <div className="container-fluid" >

              <img className="card" src="/img/editarUser.png" style={{ width: 90, height: 90 }} />
            </div>
            <br />
            <form className="user" onSubmit={handleSubmit(sendData)}>

              {/*Ingresar nombre y apellido*/}
              <div className="row mb-4">
                <div className="col">
                  <input {...register('nombres')} name="nombres" id="nombres" className={`form-control ${errors.nombres ? 'is-invalid' : ''}`} placeholder='Ingrese los nombres' />
                  <div className='alert alert-danger invalid-feedback'>{errors.nombres?.message}</div>
                </div>
                <div className="col">
                  <input {...register('apellidos')} name="apellidos" id="apellidos" className={`form-control ${errors.apellidos ? 'is-invalid' : ''}`} placeholder='Ingrese los apellidos' />
                  <div className='alert alert-danger invalid-feedback'>{errors.apellidos?.message}</div>
                </div>
              </div>

              {/*Ingresar cedula y telefono*/}
              <div className="row mb-4">
                <div className="col">
                  <input {...register('cedula')} name="cedula" id="cedula" className={`form-control ${errors.cedula ? 'is-invalid' : ''}`} placeholder='Ingrese nro cedula' />
                  <div className='alert alert-danger invalid-feedback'>{errors.cedula?.message}</div>
                </div>
                <div className="col">
                  <input {...register('telefono')} name="telefono" id="telefono" className={`form-control ${errors.telefono ? 'is-invalid' : ''}`} placeholder='Ingrese nro telefono' />
                  <div className='alert alert-danger invalid-feedback'>{errors.telefono?.message}</div>
                </div>
              </div>

              {/*Ingresar clave*/}
              <div className="row mb-4">
                <div className="col">
                  <input {...register('clave')} name="clave" id="clave" className="form-control" placeholder='Ingrese una clave' />

                </div>
              </div>

              {/* Seleccionar rol*/}
              {rol === "Administrador" && (
                <div className="row mb-4">
                  <div className="col">
                    <input {...register('correo')} name="correo" id="correo" className={`form-control ${errors.correo ? 'is-invalid' : ''}`} placeholder='Ingrese correo electronico' />
                    <div className='alert alert-danger invalid-feedback'>{errors.correo?.message}</div>
                  </div>
                  <div className="col">
                    <select {...register('rol')} name="rol" id="rol" className={`form-control ${errors.rol ? 'is-invalid' : ''}`}>
                      <option>Elija un rol</option>
                      {roles.map((aux, i) => (
                        <option key={i} value={aux.external_id} >
                          {`${aux.nombre}`}
                        </option>
                      ))}
                    </select>
                    <div className='alert alert-danger invalid-feedback'>{errors.rol?.message}</div>
                  </div>

                </div>
              )}
              <hr />
              {rol === "Administrador" && (
                <>
                  <Link href="/usuarios" className="btn btn-danger" style={{ flex: '1', marginRight: '4px' }}>
                    Cancelar
                  </Link>
                </>
              )}
              {rol === "Usuario" && (
                <>
                  <Link href="/perfilUsuario" className="btn btn-danger" style={{ flex: '1', marginRight: '4px' }}>
                    Cancelar
                  </Link>
                </>
              )}
              <button type="submit" className="btn btn-success" style={{ flex: '1', marginLeft: '4px' }}>
                Guardar
              </button>

            </form>

          </div>
        </div>
      </center>
      <br />
      <Footer />
    </div>

  );
}