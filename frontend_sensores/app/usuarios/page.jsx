'use client'
import Link from "next/link";
import Menu from '@/componentes/menu';
import Footer from '@/componentes/footer';
import mensajes from "@/componentes/Mensajes";
import { busquedaSensores, cambiarEstado, obtenerP } from "@/hooks/Conexion";
import { borrarSesion, getRol, getToken } from "@/hooks/SessionUtilClient";
import { useState } from "react";
import Cookies from "js-cookie";
import { useRouter } from "next/navigation";

export default function Page() {
    const key = getToken();
    const rol = getRol();

    const router = useRouter();
    const [obt, setObt] = useState(false);
    const [personas, setPersonas] = useState([]);
    const [valorBusqueda, setValorBusqueda] = useState('');

    const handleValorBusqueda = (event) => {
        const nuevoValor = event.target.value;
        setValorBusqueda(nuevoValor);
    };

    if (!obt) {
        obtenerP('admin/usuario', key, rol).then((info) => {

            if (info.code === 200) {
                setPersonas(info.datos);
                setObt(true);
            } else if (info.code !== 200 && (info.tag === "token expirado o no valido" || info.tag === "token no valido" || info.tag === "no existe token")) {
                mensajes(info.tag, "Error", "error");
                Cookies.remove("token");
                borrarSesion();
                router.push("/login")
            } else if (info.code !== 200 && info.tag === "Acceso no autorizado") {
                router.push("/principal")
                mensajes(info.tag, "Informacion", "error");
            } else {
                mensajes("No se pudo Listar los Usuarios", "Error", "error");
            }
        });
    };

    //Metodo para busqueda y listado de datos
    const handleBuscarClick = () => {

        const selectElement = document.getElementById('filtroBusqueda');
        const selectedValue = selectElement.value;
        let dir = "";

        if (selectedValue === "cedula") {
            if (valorBusqueda.length === 0) {
                mensajes("ingresa algun valor de busqueda", "Informacion", "error");
                return;
            } else {
                dir = "admin/usuario/buscar/cedula/" + valorBusqueda;
            }
        } else if (selectedValue === "nombre") {
            const palabra = valorBusqueda.split(' ');
            if (palabra.length >= 2) {
                dir = "admin/usuario/buscar/nombre/" + valorBusqueda;
            } else {
                mensajes("ingresa los dos nombres", "Informacion", "error");
                return;
            }
        } else {
            dir = "admin/usuario";
        }

        busquedaSensores(dir, key, rol).then((info) => {
            if (info.code === 200) {
                setPersonas(info.datos);
            } else if (info.code !== 200 && (info.tag === "token expirado o no valido" || info.tag === "token no valido" || info.tag === "no existe token")) {
                mensajes(info.tag, "Error", "error");
                Cookies.remove("token");
                borrarSesion();
                router.push("/login")
            } else if (info.code !== 200 && info.tag === "Acceso no autorizado") {
                router.push("/principal")
                mensajes(info.tag, "Informacion", "error");
            } else {
                mensajes("No se pudo listar los datos", "Error", "error");
            }
        });
    };

    //cambiar estado sensor
    const handleEstado = (external, dato) => {

        let datos = {
            'estado': dato,
        }

        cambiarEstado('admin/usuario/estado/' + external, datos, key, rol).then((info) => {

            if (info.code === 200) {
                mensajes(info.tag, "Informacion", "success");
                setObt(false);
            } else if (info.code !== 200 && (info.tag === "token expirado o no valido" || info.tag === "token no valido" || info.tag === "no existe token")) {
                mensajes(info.tag, "Error", "error");
                Cookies.remove("token");
                borrarSesion();
                router.push("/login")
            } else if (info.code !== 200 && info.tag === "Acceso no autorizado") {
                router.push("/principal")
                mensajes(info.tag, "Informacion", "error");
            } else {
                mensajes(info.tag, "Error", "error");
            }
        });
    };

    return (
        <div className="row">
            <Menu />
            <h1 style={{ textAlign: "center" }}>Usuarios</h1>
            <div className="container-fluid">
                <div className="input-group mb-3 ">
                    <select className="form-control w-20" id="filtroBusqueda" style={{ backgroundColor: '#F2F6F5', color: '#333', border: "1px solid #000000" }} aria-describedby="button-addon2">
                        <option value="">Todo</option>
                        <option value="cedula">Cedula</option>
                        <option value="nombre">Nombre</option>

                    </select>
                    <input type="text" className="form-control w-50" placeholder="Ingrese dato de busqueda" onChange={handleValorBusqueda} value={valorBusqueda} />
                    <button className="btn btn-secondary " type="button" id="button-addon2" onClick={handleBuscarClick}>Buscar</button>
                </div>
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-12 mb-4 text-center">
                            <div className="btn-group" role="group">
                                <Link href="/usuarios/registrar" className="btn btn-success font-weight-bold">Registrar</Link>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-12">
                    <table className="table table-bordered" style={{ borderColor: "ActiveBorder" }}>
                        <thead className="table-active">
                            <tr style={{ textAlign: "center", backgroundColor: "black", color: "white" }}>
                                <th>Nro</th>
                                <th>NOMBRES</th>
                                <th>APELLIDOS</th>
                                <th>TELEFONO</th>
                                <th>CEDULA</th>
                                <th>CORREO</th>
                                <th>ESTADO</th>
                                <th>ROL</th>
                                <th>ACCIONES</th>
                            </tr>
                        </thead>
                        <tbody>
                            {personas && personas.length > 0 ? (
                                personas.map((dato, index) => (
                                    <tr key={index}>
                                        <td>{index + 1}</td>
                                        <td>{dato.nombres}</td>
                                        <td>{dato.apellidos}</td>
                                        <td>{dato.telefono}</td>
                                        <td>{dato.cedula}</td>
                                        <td>{dato.cuenta.correo}</td>
                                        <td>{dato.cuenta?.estado === true ? 'Activo' : 'Inactivo'}</td>
                                        <td>{dato.rol.nombre}</td>
                                        <td>
                                            {<Link style={{ marginRight: "5px" }} href="/usuarios/modificar/[external]" as={`usuarios/modificar/${dato.external_id}`} className="btn btn-warning font-weight-bold">Modificar</Link>}
                                            {dato.rol.nombre === "Usuario" && (
                                                <>
                                                    {dato.cuenta.estado ? (
                                                        <button type="button" onClick={() => handleEstado(dato.external_id, "false")} style={{ margin: "5px" }} className="btn btn-danger font-weight-bold"> Dar de baja </button>
                                                    ) : (
                                                        <button type="button" onClick={() => handleEstado(dato.external_id, "true")} style={{ margin: "5px" }} className="btn btn-success font-weight-bold"> Activar </button>
                                                    )}
                                                </>
                                            )}

                                        </td>
                                    </tr>
                                ))
                            ) : (
                                <tr>
                                    <td colSpan="8">No se dispone de información</td>
                                </tr>
                            )}

                        </tbody>
                    </table>
                </div>
            </div>
            <Footer />
        </div >
    );
}






