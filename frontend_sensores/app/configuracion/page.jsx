'use client';
import * as Yup from 'yup';
import { useRouter } from 'next/navigation';
import { borrarSesion, getExternal, getRol, getToken } from "@/hooks/SessionUtilClient";
import Menu from '@/componentes/menu';
import Footer from '@/componentes/footer';
import Cookies from "js-cookie";
import mensajes from "@/componentes/Mensajes";
import { useEffect, useState } from "react";
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm } from 'react-hook-form';
import { guardar, obtenerData } from '@/hooks/Conexion';

export default function Page() {

    const key = getToken();
    const external = getExternal();
    const rol = getRol();

    const router = useRouter();
    const [esp, setEsp] = useState([]);
    const [obt, setObt] = useState(false);

    const validationShema = Yup.object().shape({
        ip: Yup.string().test('is-valid-ip', 'Ingrese una dirección IP válida', function (value) {
            return /^(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)$/.test(value);
        }).required('Ingrese una dirección IP'),
    });

    const formOptions = { resolver: yupResolver(validationShema) };
    const { register, handleSubmit, formState, setValue } = useForm(formOptions);
    const { errors } = formState;

    if (!obt) {
        obtenerData('admin/config', key, rol).then((info) => {
            if (info.code === 200 && info.datos.length > 0) {
                setEsp(info.datos[0]);
                setObt(true);
            } else if (info.code !== 200 && (info.tag === "token expirado o no valido" || info.tag === "token no valido" || info.tag === "no existe token")) {
                mensajes(info.tag, "Error", "error");
                Cookies.remove("token");
                borrarSesion();
                router.push("/login")
            } else if (info.code !== 200 && info.tag === "Acceso no autorizado") {
                router.push("/principal")
                mensajes(info.tag, "Informacion", "error");
            } else if (info.code === 200) {
                mensajes("No existe configuracion del ESP32 Maestro", "Informacion", "error");
                setObt(true);
            } else {
                mensajes("No se pudo listar los datos", "Error", "error");
            };
        });
    }

    //Metodo para guardar la configuracion
    const sendData = (data) => {

        var datos = {
            'ip': data.ip,
            'external': esp ? esp.external_id : undefined
        };

        guardar('admin/config/guardar', datos, key, rol).then((info) => {
            if (info.code === 200) {
                mensajes("Configuracion guardada correctamente", "Informacion", "success")
                setObt(false);
            } else if (info.code !== 200 && (info.tag === "token expirado o no valido" || info.tag === "token no valido" || info.tag === "no existe token")) {
                mensajes(info.tag, "Error", "error");
                Cookies.remove("token");
                borrarSesion();
                router.push("/login")
            } else if (info.code !== 200 && info.tag === "Acceso no autorizado") {
                router.push("/principal")
                mensajes(info.tag, "Informacion", "error");
            } else {
                mensajes("Configuracion no se pudo guardar", "Error", "error")
            }
        });
    };

    useEffect(() => {
        if (esp) {
            setValue('ip', esp.ip);
            setValue('intervalo', esp.intervalo);
        }
    }, [esp, setValue]);

    return (
        <div className="row" >
            <Menu />
            <center>
                <div className="container py-5 h-100">
                    <div className="row d-flex justify-content-center align-items-center h-100">
                        <div className="col col-lg-8 mb-4 mb-lg-0">
                            <div className="card mb-3" style={{ borderRadius: ".5rem" }}>
                                <div className="row g-0">
                                    <div className="col-md-4 gradient-custom text-center text-white"
                                        style={{
                                            border: '0.5rem', borderBottomLeftRadius: '0.5rem', background: `linear-gradient(to right bottom, rgba(90, 91, 92, 1) ,rgba(235, 244, 253,1 ))`
                                        }}>
                                        <img src="/img/configuracion Esp32.jpg"
                                            alt="Avatar" className="img-fluid my-5" style={{ width: "150px" }} />
                                        <i className="far fa-edit mb-5"></i>

                                    </div>
                                    <div className="col-md-8 " style={{ background: `linear-gradient(to right bottom, rgba(240, 240, 240,1), rgba(240, 240, 240,1))` }}>
                                        <div className="card-body p-4">
                                            <h6>Configuracion del ESP32 Maestro</h6>
                                            <hr className="mt-0 mb-4" />

                                            <form className="user" onSubmit={handleSubmit(sendData)}>
                                                <div className="row pt-1">
                                                    <div className="col-6 mb-3">
                                                        <h6>Direccion IP: </h6>
                                                        <input {...register('ip')} name="ip" id="ip" style={{ width: '200%', border: '1px solid gray' }} className={`form-control ${errors.ip ? 'is-invalid' : ''}`} placeholder='Ingrese direccion ip' />
                                                        <div className='alert alert-danger invalid-feedback'>{errors.ip?.message}</div>
                                                    </div>
                                                </div>
                                                <hr />


                                                <br />
                                                <button type="submit" className="btn btn-success" style={{ flex: '1', marginLeft: '4px' }}>
                                                    Guardar Configuracion
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </center>
            <br />
            <Footer />
        </div>

    );

}
