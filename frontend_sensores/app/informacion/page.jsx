'use client'
import Menu from '@/componentes/menu';
import Footer from '@/componentes/footer';
import {
    FacebookIcon,
    PinterestIcon,
    RedditIcon,
    WhatsappIcon,
    LinkedinIcon,
} from 'next-share';

export default function Page() {

    const listItemStyle = {
        display: 'flex',
        alignItems: 'center',
        fontFamily: 'Times New Roman, serif',
        fontSize: '16px',
        lineHeight: '1.5',
        color: '#000',
        marginBottom: '8px',
    };

    const listContainerStyle = {
        listStyleType: 'none',
        padding: '0',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    };

    return (

        <div className="container-fluid p-0">
            <Menu />
            <center>
                <div className='container-fluid' >
                    <h1 className="my-4 display-5 fw-bold ls-tight" style={{ color: 'hsl(218, 81%, 75%)' }}>
                        GRUPO E
                    </h1>

                    <ul style={listContainerStyle}>
                        <li style={listItemStyle}><span style={{ marginRight: '8px' }}>•</span>Cristian Yaguana</li>
                        <li style={listItemStyle}><span style={{ marginRight: '8px' }}>•</span>Lizbeth Quezada</li>
                        <li style={listItemStyle}><span style={{ marginRight: '8px' }}>•</span>Alexander Cañar</li>
                        <li style={listItemStyle}><span style={{ marginRight: '8px' }}>•</span>Luis Narvaez</li>
                    </ul>

                    <br />
                    <p className='' style={{ textAlign: 'center' }}>
                        El siguiente sistema ayuda a obtener una prediccion del clima en base a
                        la recoleccion de datos de las motas climaticas las cuales estan compuestas
                        por dispositivos esp32 y sensores de diferentes tipos para tratar de hacer
                        una prediccion lo mas cerca de la realidad.
                    </p>
                    <br />

                    <h1 style={{ fontSize: "1.5em", color: '#333', fontFamily: 'Arial, sans-serif', marginBottom: '20px', }}>Redes Sociales</h1>

                    <div className='container-fluid'>
                        <a href={'https://www.facebook.com/'} style={{ marginRight: '20px' }} target="_blank" rel="noopener noreferrer">
                            <FacebookIcon size={40} round />
                        </a>

                        <a href={''} style={{ marginRight: '20px' }} target="_blank" rel="noopener noreferrer">
                            <PinterestIcon size={40} round />
                        </a>

                        <a href={''} style={{ marginRight: '20px' }} target="_blank" rel="noopener noreferrer">
                            <RedditIcon size={40} round />
                        </a>

                        <a href={''} style={{ marginRight: '20px' }} target="_blank" rel="noopener noreferrer">
                            <WhatsappIcon size={40} round />
                        </a>

                        <a href={''} style={{ marginRight: '20px' }} target="_blank" rel="noopener noreferrer">
                            <LinkedinIcon size={40} round />
                        </a>
                    </div>

                </div>
            </center>
            <br />
            <Footer />
        </div>
    )
}