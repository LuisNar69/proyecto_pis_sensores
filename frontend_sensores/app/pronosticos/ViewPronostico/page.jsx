'use client';
import Menu from '@/componentes/menu';
import Footer from '@/componentes/footer';
import { useRouter } from "next/navigation";
import { useState, useEffect } from "react";
import { obtenerPronostico } from '@/hooks/Conexion';
import mensajes from '@/componentes/Mensajes';
import { borrarSesion, getRol, getToken } from '@/hooks/SessionUtilClient';
import Cookies from 'js-cookie';
import { Chart } from "chart.js/auto";
import { format } from 'date-fns';
import React from 'react';


export default function Page() {

    const token = getToken();
    const rol = getRol();

    const router = useRouter();
    const [temp, setTemp] = useState([]);
    const [hume, setHume] = useState([]);
    const [datosS, setDatosS] = useState([]);
    const [llamada2, setLlamada2] = useState(false);
    const [llamada3, setLlamada3] = useState(false);
    const [llamada, setLlamada] = useState(false);


    //Metodo para listar datos en diagrama
    useEffect(() => {
        if (!llamada2) {
            obtenerPronostico('pronosticos/obtener', token, rol).then((info) => {

                if (info.code === 200) {
                    setTemp(info.temperaturasExtrapoladas);
                    setLlamada2(true);
                } else if (info.code !== 200 && (info.tag === "token expirado o no valido" || info.tag === "token no valido" || info.tag === "no existe token")) {
                    mensajes(info.tag, "Error", "error");
                    Cookies.remove("token");
                    borrarSesion();
                    router.push("/login")
                } else if (info.code !== 200 && info.tag === "Acceso no autorizado") {
                    router.push("/principal")
                    mensajes(info.tag, "Informacion", "error");
                } else {
                    mensajes("No se pudo obtener pronostico", "Error", "error");
                }
            });
        };
    }, [llamada2]);

    useEffect(() => {
        if (!llamada) {
            obtenerPronostico('pronosticos/obtenerH', token, rol).then((info) => {
                if (info.code === 200) {
                    setHume(info.humedadesExtrapoladas);
                    setLlamada(true);
                } else {
                    mensajes("No se pudo obtener pronostico", "Error", "error");
                }
            });
        };
    }, [llamada]);

    useEffect(() => {
        var ctx = document.getElementById('myChart').getContext('2d');
        Chart.getChart(ctx)?.destroy();

        const horaActual = format(new Date(), 'HH');
        const labels = [];
        const horasEnElFuturo = 24;


        for (let i = horaActual; i < horasEnElFuturo; i++) {

            if (!isNaN(horaActual)) {
                labels.push(i + ":00");
            }
        }

        const datosT = temp.map(dato => parseFloat(dato));
        const datosH = hume.map(dato => parseFloat(dato));


        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: [{
                    data: datosT,
                    label: "Temperatura",
                    borderColor: "#3cba9f",
                    backgroundColor: "#71d1bd",
                    fill: false,
                }, {
                    data: datosH,
                    label: "Humedad",
                    borderColor: "#ffa500",
                    backgroundColor: "#ffc04d",
                    fill: false,
                }
                ]
            },
        });
    });

    useEffect(() => {
        if (!llamada3) {
            obtenerPronostico('admin/catalogo', token, rol).then((info) => {
                if (info.code === 200) {
                    setDatosS(info.datos);
                    setLlamada3(true);
                } else {
                    mensajes("No se pudo obtener pronostico", "Error", "error");
                }
            });
        };
    }, [llamada3]);

    return (
        <div className="row">
            <Menu />
            <br />
            <br />
            <div className="d-flex flex-column align-items-center">
                <h1 style={{ textAlign: "center", fontSize: "1.5em" }}>Pronosticos</h1>
            </div>
            <br />

            <div className="flex mx-auto my-auto" style={{ height: 700, width: 1000 }}>
                <div className='border border-gray-400 pt-0 rounded-xl  w-full h-fit my-auto  shadow-xl'>
                    <canvas id='myChart' style={{ height: 100, width: 100 }}></canvas>
                </div>
                <br />
                <div className="col-12 " style={{ maxHeight: '375px', overflowY: 'auto' }}>

                    <table className="table table-bordered" style={{ borderColor: "ActiveBorder" }}>
                        <thead className="table-active">
                            <tr style={{ textAlign: "center", backgroundColor: "black", color: "white" }}>
                                <th >CLIMA</th>
                                <th >HUMEDAD</th>
                                <th >TEMPERATURA</th>
                                <th >DESCRIPCION</th>
                                <th >RANGO</th>
                            </tr>
                        </thead>
                        <tbody>
                            {temp && temp.length > 0 ? (
                                temp.map((tempDato, i) => (
                                    datosS.map((elemento, j) => {
                                        const humDato = hume[i]; // Obtener el dato de humedad correspondiente a la misma posición en el array

                                        console.log(temp)
                                        const datoEnRango = tempDato >= elemento.rango_minimo && tempDato <= elemento.rango_maximo;

                                        if (datoEnRango) {
                                            return (
                                                <tr key={`${i}-${j}`}>
                                                    <td>{elemento.nombre}</td>
                                                    <td>{humDato}</td>
                                                    <td>{tempDato}</td>
                                                    <td>{elemento.informacion}</td>
                                                    <td>{elemento.rango_minimo} - {elemento.rango_maximo}</td>
                                                </tr>
                                            );
                                        } else {
                                            return null;
                                        }

                                    })
                                ))
                            ) : (
                                <tr>
                                    <td colSpan="5">No se dispone de información</td>
                                </tr>
                            )}
                        </tbody>




                    </table>
                </div>
            </div>

            <br />
            <Footer />
        </div>
    );
}