'use client';
import Menu from '@/componentes/menu';
import Footer from '@/componentes/footer';
import { useRouter } from "next/navigation";
import { useState, useEffect } from "react";
import { busquedaDatoRecolectado, obtenerData, obtenerPronostico } from '@/hooks/Conexion';
import mensajes from '@/componentes/Mensajes';
import Link from "next/link";
import { borrarSesion, getRol, getToken } from '@/hooks/SessionUtilClient';
import Cookies from 'js-cookie';


export default function Page() {

    const token = getToken();
    const rol = getRol();

    const router = useRouter();
    const [datosS, setDatosS] = useState([]);
    const [llamada, setLlamada] = useState(false);
    const [valorBusqueda, setValorBusqueda] = useState('');
    const valFecha = /^\d{4}\-\d{2}\-\d{2}$/;

    const handleValorBusqueda = (event) => {
        setValorBusqueda(event.target.value);
    };

    //Metodo para listar datos
    useEffect(() => {
        if (!llamada) {
            obtenerData('sensores/', token, rol).then((info) => {
                if (info.code === 200) {
                    setDatosS(info.datos);
                    setLlamada(true);
                } else if (info.code !== 200 && (info.tag === "token expirado o no valido" || info.tag === "token no valido" || info.tag === "no existe token")) {
                    mensajes(info.tag, "Error", "error");
                    Cookies.remove("token");
                    borrarSesion();
                    router.push("/login")
                } else if (info.code !== 200 && info.tag === "Acceso no autorizado") {
                    router.push("/principal")
                    mensajes(info.tag, "Informacion", "error");
                } else {
                    mensajes("No se pudo Listar los datos", "Error", "error");
                }
            });
        };
    }, [llamada]);

    //Metodo para busqueda y listado de datos
    const handleBuscarClick = () => {

        const selectElement = document.getElementById('filtroBusqueda');
        const selectedValue = selectElement.value;
        let dir = "";

        if (selectedValue === "tipo_sensor") {
            dir = "sensores/buscar/tipo/" + valorBusqueda.toUpperCase();
        } else if (selectedValue === "fecha") {
            if (valFecha.test(valorBusqueda)) {
                dir = "sensores/buscar/fecha/" + valorBusqueda;
            } else {
                mensajes("por favor, ingresa fecha con formato YYYY-MM-DD", "Informacion", "error");
                return;
            }
        } else if (selectedValue === "nombre") {
            dir = "sensores/buscar/nombre/" + valorBusqueda;
        } else {
            dir = "sensores/";
        }

        busquedaDatoRecolectado(dir, token, rol).then((info) => {
            if (info.code === 200) {
                setDatosS(info.datos);
            } else if (info.code !== 200 && (info.tag === "token expirado o no valido" || info.tag === "token no valido" || info.tag === "no existe token")) {
                mensajes(info.tag, "Error", "error");
                Cookies.remove("token");
                borrarSesion();
                router.push("/login")
            } else if (info.code !== 200 && info.tag === "Acceso no autorizado") {
                router.push("/principal")
                mensajes(info.tag, "Informacion", "error");
            } else {
                mensajes("No se pudo listar los datos", "Error", "error");
            }
        });
    };

    //mensaje de informacion
    const handleMouseOver = () => {
        mensajes('formato para busqueda por fecha YYYY-MM-DD', "", "");
    };

    return (
        <div className="row">
            <Menu />
            <br />
            <div className="d-flex flex-column align-items-center">
                <h1 style={{ textAlign: "center", fontSize: "1.5em" }}>Datos Sensores</h1>

                <div className="input-group mb-3 ">
                    <select className="form-control w-20" id="filtroBusqueda" style={{ backgroundColor: '#F2F6F5', color: '#333', border: "1px solid #000000" }} aria-describedby="button-addon2">
                        <option value="">Hoy</option>
                        <option value="tipo_sensor">Tipo Sensor</option>
                        <option value="nombre">Nombre Sensor</option>
                        <option value="fecha">Fecha</option>
                    </select>
                    <input type="text" className="form-control w-50" placeholder="Ingrese dato de busqueda" onChange={handleValorBusqueda} value={valorBusqueda} />

                    <button type="button" className="close" onMouseOver={handleMouseOver}
                        style={{ backgroundColor: "orange", margin: '5px', borderRadius: '50%', width: '30px', height: '30px', padding: '0', fontSize: '16px' }}>
                        ?
                    </button>

                    <button className="btn btn-secondary " type="button" id="button-addon2" onClick={handleBuscarClick}>Buscar</button>
                </div>
                <div>
                    <Link style={{ margin: "15px" }} href="/pronosticos/ViewPronostico" className="btn btn-success font-weight-bold" > Ver Pronostico</Link>
                </div>
            </div>


            <div className="col-12 " style={{ maxHeight: '400px', overflowY: 'auto' }}>
                <table className="table table-bordered" style={{ borderColor: "ActiveBorder" }}>
                    <thead className="table-active">
                        <tr style={{ textAlign: "center", backgroundColor: "black", color: "white" }}>
                            <th >SENSOR</th>
                            <th >TIPO</th>
                            <th >DATO</th>
                            <th >FECHA</th>
                            <th >HORA</th>

                        </tr>
                    </thead>
                    <tbody>

                        {datosS && datosS.length > 0 ? (
                            datosS.map((dato, i) => (
                                dato.datoRecolectado.map((recolectado, j) => (
                                    <tr key={`${i}-${j}`}>
                                        <td>{dato.nombre}</td>
                                        <td>{dato.tipo_sensor}</td>
                                        <td>{recolectado.dato}</td>
                                        <td>{recolectado.fecha}</td>
                                        <td>{recolectado.hora}</td>
                                    </tr>
                                ))
                            ))
                        ) : (
                            <tr>
                                <td colSpan="5">No se dispone de información</td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>


            <br />
            <br />
            <Footer />
        </div>


    );
}